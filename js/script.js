const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 500;

ctx.font = '50px Georgia';

// Mouse interactivity
let canvasPosition = canvas.getBoundingClientRect();

const mouse = {
    x: canvas.width/2,
    y: canvas.height/2,
    click: false
};

function canvasApp() {
  const pointImage = new Image();
  pointImage.src = 'img/point.png';

  function drawScreen() {
    // Clear Canvas
    ctx.fillStyle = '#EEE';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = '#000';
    ctx.strokeRect(1, 1, canvas.width - 2, canvas.height - 2);

    // Change Ball Coords
    if (pos2.x - ball.x > 1 && pos2.y - ball.y > 1) {
      ball.x += xUnits;
      ball.y += yUnits;
    }

    // Draw point to path
    points.push({x: ball.x, y: ball.y});

    for (let i = 0; i < points.length; i++) {
      ctx.drawImage(pointImage, points[i].x, points[i].y, 1, 1);
    }

    // Draw Ball
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, 15, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();

    // Draw Target Point
    ctx.fillStyle = '#FF0000';
    ctx.beginPath();
    ctx.arc(pos2.x, pos2.y, 3, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();

    // Draw Start Point
    ctx.fillStyle = '#00FF00';
    ctx.beginPath();
    ctx.arc(pos1.x, pos1.y, 3, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();
  }



  const speed = 5;
  const pos1 = {x: canvas.width/2, y: canvas.height/2};
  const pos2 = {x: 50, y: 120};
  
  const radians = Math.atan((pos2.y - pos1.y) / (pos2.x - pos1.x));
  console.log(radians)
  const xUnits = Math.cos(radians) * speed;
  const yUnits = Math.sin(radians) * speed;

  const ball = {x: pos1.x, y: pos1.y};
  const points = new Array();

  function gameLoop() {
    //window.setTimeout(gameLoop, 200);
    drawScreen();
    requestAnimationFrame(gameLoop);
  }
  gameLoop();
}
canvasApp();

