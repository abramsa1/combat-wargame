const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 500;

const targetCoords = [];

let score = 0;
let gameFrame = 0;
let activeEntity = null;

ctx.font = '50px Georgia';

let canvasPosition = canvas.getBoundingClientRect();

const mouse = {
    x: canvas.width/2,
    y: canvas.height/2,
    click: false
};

const rightMouse = {
  x: canvas.width/2,
  y: canvas.height/2,
  click: false
};



const planeImage = new Image();
planeImage.src = 'img/Su27-2.png';
// Plane
class Plane {
  constructor(name, id) {
    this.x = canvas.width/2;
    this.y = canvas.height/2;
    this.radius = 30;
    this.speed = 1;
    this.angle = 0;

    this.active = false;
    this.info = false;

    this.health = 100;
    this.distance;
    this._id = id;
    this.name = name;
    this.targetPoint = {x: this.x, y: this.y};
    this.delta = {x: 0, y: 0};

    this.xUnits = 0;
    this.yUnits = 0;

    this.radar = new Radar('Н001 Меч', 150, true);
    this.rocket = new Rocket('РВВ-АВ', '001');

    this.key = 1;
    this.moves = 0;
    this.status = 'active';

    this.patrol = {};
  }
  setStatus(str) {
    if (this.active) {
      this.status = str;
    }
  }
  setActive() {
    const dx2 = this.x - mouse.x;
    const dy2 = this.y - mouse.y;
    this.distance2 = Math.sqrt(dx2**2 + dy2**2);
    this.active = this.distance2 < this.radius;
    if (this.active) {
      activeEntity = this._id;
      //this.showInfo();
    } else {
      activeEntity = null;
      //this.clearInfo();
    }
    console.log(activeEntity);
  }
  setTargetPoint() {
    if (activeEntity === this._id) {
      this.targetPoint.x = rightMouse.x;
      this.targetPoint.y = rightMouse.y;

      const dx = this.targetPoint.x - this.x;
      const dy = this.targetPoint.y - this.y;

      let theta = Math.atan2(dy, dx) - Math.PI;
      this.angle = theta;
      console.log(this.angle);
    
      const distance = Math.sqrt(dx**2 + dy**2);
      this.moves = distance / this.speed;
      this.xUnits = dx / this.moves;
      this.yUnits = dy / this.moves;
    }
  }
  showInfo() {
    const container = document.querySelector('.info');
    const title = document.createElement('h5');
    const text = document.createElement('p');
    title.textContent = this.name;
    text.textContent = this.radar.name;
    
    container.append(title, text);
  }
  clearInfo() {
    const container = document.querySelector('.info');
    container.textContent = '';
  }

  update() {
    // Change Coordinates
    if (this.moves > 0) {
      this.moves--;
      this.x += this.xUnits;
      this.y += this.yUnits;
    }
  }

  draw() {
    // Draw Radar Circle
    this.radar.draw(this.x, this.y);

    // Draw Target Point
    ctx.fillStyle = '#0000FF';
    ctx.beginPath();
    ctx.arc(this.targetPoint.x, this.targetPoint.y, 3, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();

    // Plane Circle
    if (this.active) {
      ctx.fillStyle = 'green';
    } else {
      ctx.fillStyle = '#FF0000';
    }
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
    ctx.fill();
    ctx.closePath();

    // Check Rocket Launch
    if (this.status === 'attack') {
      this.rocket.launch(this.x, this.y, this.targetPoint.x, this.targetPoint.y);
      ctx.strokeStyle = 'red';
      this.status = 'active';
    } else {
      ctx.strokeStyle = 'lightgrey';
    }
    this.rocket.draw(this.x, this.y);

    // Draw Moving Line
    ctx.lineWidth  = 0.8;
    ctx.beginPath();
    ctx.moveTo(this.x, this.y);
    ctx.lineTo(this.targetPoint.x, this.targetPoint.y);
    ctx.stroke();

    // Rotate Plane
    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.angle);
    ctx.drawImage(planeImage, -planeImage.width/2, -planeImage.height/2);
    ctx.restore();
  }
}

class Radar {
  constructor(name, radius, status) {
    this.name = name,
    this.radius = radius;
    this.status = status;
  }
  draw(x, y) {
    if (this.status) {
      ctx.beginPath();
      ctx.arc(x, y, this.radius, 0, Math.PI * 2);
      ctx.stroke();
      ctx.closePath();
    }
  }
}

const rocketImage = new Image();
rocketImage.src = 'img/rvv2.png';
class Rocket {
  constructor(name, id) {
    this._id = id
    this.name = name;
    this.speed = 2;

    this.xTarget = 0;
    this.yTarget = 0;

    this.x = 0;
    this.y = 0;

    this.angle = 0;

    this.moves = 0;
    this.xUnits = 0;
    this.yUnits = 0;

    this.fire = false;
  }
  launch(startX, startY, targetX, targetY) {
    this.targetX = targetX;
    this.targetY = targetY;

    this.x = startX;
    this.y = startY;

    const dx = this.targetX - this.x;
    const dy = this.targetY - this.y;

    const theta = Math.atan2(dy, dx) - Math.PI;
    this.angle = theta;
  
    const distance = Math.sqrt(dx**2 + dy**2);
    this.moves = distance / this.speed;
    this.xUnits = dx / this.moves;
    this.yUnits = dy / this.moves;

    this.fire = true;
  }
  draw(x, y) {
    if (!this.fire) {
      this.x = x;
      this.y = y;
    }

    if (this.moves > 0) {
      this.moves--;
      this.x += this.xUnits;
      this.y += this.yUnits;
    } else {
      this.fire = false;
    }

    // Rocket POINT
    // ctx.fillStyle = 'orange';
    // ctx.beginPath();
    // ctx.arc(this.x, this.y, 5, 0, Math.PI * 2);
    // ctx.fill();
    // ctx.closePath();

    // Rotate Rocket and Render
    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.angle);
    ctx.drawImage(rocketImage, 0 - rocketImage.width/2, - rocketImage.height/2);
    ctx.restore();
  }
}

class TrainingTarget {
  constructor(id, x, y) {
    this._id = id,
    this.radius = 5;
    this.x = x;
    this.y = y;
    this.status = true;
    //this.status = status;
  }
  draw() {
    if (this.status) {
      ctx.fillStyle = 'black';
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
      ctx.fill();
      ctx.closePath();
    }
  }
}

canvas.addEventListener('mousedown', e => {
  if (e.which === 1) {
    mouse.click = true;
    mouse.x = e.x - canvasPosition.left;
    mouse.y = e.y - canvasPosition.top;
    plane.setActive();
  } else if (e.which === 3) {
    rightMouse.click = true;
    rightMouse.x = e.x - canvasPosition.left;
    rightMouse.y = e.y - canvasPosition.top; // 18 - cursor height
    plane.setTargetPoint();
    checkBattle();
  }
});

function checkBattle() {
  console.log('Check Battle');
  targetCoords.forEach(coords => {
    const dx = coords.x - rightMouse.x;
    const dy = coords.y - rightMouse.y;
    const distance = Math.sqrt(dx**2 + dy**2);
    if (distance < 5) {
      console.log('Missile!');
      plane.setStatus('attack');
    }
  })
}

window.addEventListener('mouseup', e => {
  mouse.click = false;
  rightMouse.click = false;
});
canvas.addEventListener("contextmenu", e => e.preventDefault());

const plane = new Plane('Su-27SM', 0);
const target = new TrainingTarget('t001', 50, 50);
targetCoords.push({x: 50, y: 50});

function animate() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  plane.update();
  plane.draw();
  target.draw();
  requestAnimationFrame(animate);
}

animate();