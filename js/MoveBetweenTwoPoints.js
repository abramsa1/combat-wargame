const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 500;

ctx.font = '50px Georgia';

// Mouse interactivity
const canvasPosition = canvas.getBoundingClientRect();

const mouse = {
    x: canvas.width/2,
    y: canvas.height/2,
    click: false
};

canvas.addEventListener('mousedown', (e) => {
  mouse.click = true;
  mouse.x = e.x - canvasPosition.left;
  mouse.y = e.y - canvasPosition.top;
  const canvasObj = canvasApp();
  canvasObj.update();
});
canvas.addEventListener('mouseup', (e) => {
  mouse.click = false;
});

const speed = 5;
const ball = {x: 20, y: 250};
const pos1 = {x: ball.x, y: ball.y};
const points = new Array();
let moves = 0, xUnits = 0, yUnits = 0;

function canvasApp() {
  const pointImage = new Image();
  pointImage.src = 'point.png';

  function drawScreen() {
    ctx.fillStyle = '#EEE';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    // Box
    ctx.strokeStyle = '#000';
    ctx.strokeRect(1, 1, canvas.width - 2, canvas.height - 2);

    // Create Ball
    if (moves > 0) {
      moves--;
      ball.x += xUnits;
      ball.y += yUnits;
    }

    // Draw point to path
    points.push({x: ball.x, y: ball.y});

    for (let i = 0; i < points.length; i++) {
      ctx.drawImage(pointImage, points[i].x, points[i].y, 1, 1);
    }
    //console.log(moves, ball.x, ball.y)
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, 15, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();
  }

  function update() {
    if (mouse.click) {
      pos1.x = ball.x;
      pos1.y = ball.y;
      console.log(mouse.x, mouse.y);
      const pos2 = {x: mouse.x, y: mouse.y};
    
      const dx = pos2.x - pos1.x;
      const dy = pos2.y - pos1.y;
    
      const distance = Math.sqrt(dx**2 + dy**2);
      moves = distance/speed;
    
      xUnits = (pos2.x - pos1.x) / moves;
      yUnits = (pos2.y - pos1.y) / moves;
    }
  }

  function gameLoop() {
    window.setTimeout(gameLoop, 10);
    drawScreen();
  }
  gameLoop();

  return { update: update };
}
canvasApp();

