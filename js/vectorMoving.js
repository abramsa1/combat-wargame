const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 500;

ctx.font = '50px Georgia';

// Mouse interactivity
let canvasPosition = canvas.getBoundingClientRect();

const mouse = {
    x: canvas.width/2,
    y: canvas.height/2,
    click: false
};

function canvasApp() {
  const pointImage = new Image();
  pointImage.src = 'point.png';

  function drawScreen() {
    // Clear Canvas
    ctx.fillStyle = '#EEE';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = '#000';
    ctx.strokeRect(1, 1, canvas.width - 2, canvas.height - 2);

    // Change Ball Coords
    ball.x += xUnits;
    ball.y += yUnits;

    // Draw point to path
    points.push({x: ball.x, y: ball.y});

    for (let i = 0; i < points.length; i++) {
      ctx.drawImage(pointImage, points[i].x, points[i].y, 1, 1);
    }

    // Draw Ball
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, 15, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();
  }

  const speed = 5;
  const pos1 = {x: 20, y: 20};

  const angle = 45;
  const radians = angle * Math.PI/180;
  // if pos2 -

  const xUnits = Math.cos(radians) * speed;
  const yUnits = Math.sin(radians) * speed;

  const ball = {x: pos1.x, y: pos1.y};
  const points = new Array();

  function gameLoop() {
    window.setTimeout(gameLoop, 200);
    drawScreen();
  }
  gameLoop();
}
canvasApp();

