const canvas = document.getElementById('canvas1');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 500;

ctx.font = '50px Georgia';

// Mouse interactivity
let canvasPosition = canvas.getBoundingClientRect();

const mouse = {
    x: canvas.width/2,
    y: canvas.height/2,
    click: false
};

function canvasApp() {
  const pointImage = new Image();
  pointImage.src = 'point.png';

  function drawScreen() {
    // Clear Canvas
    ctx.fillStyle = '#EEE';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = '#000';
    ctx.strokeRect(1, 1, canvas.width - 2, canvas.height - 2);

    // Change Ball Coords
    ball.x += xUnits;
    ball.y += yUnits;

    // Draw point to path
/*     points.push({x: ball.x, y: ball.y});

    for (let i = 0; i < points.length; i++) {
      ctx.drawImage(pointImage, points[i].x, points[i].y, 1, 1);
    } */

    // Draw Ball
    ctx.fillStyle = '#000';
    ctx.beginPath();
    ctx.arc(ball.x, ball.y, 15, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();

    // Draw Target Point
    // ctx.fillStyle = '#FF0000';
    // ctx.beginPath();
    // ctx.arc(pos2.x, pos2.y, 3, 0, Math.PI*2, true);
    // ctx.closePath();
    // ctx.fill();

    // Draw Start Point
    ctx.fillStyle = '#00FF00';
    ctx.beginPath();
    ctx.arc(pos1.x, pos1.y, 3, 0, Math.PI*2, true);
    ctx.closePath();
    ctx.fill();
    
    if (ball.x + 15 > canvas.width || ball.x - 15 < 0) {
      angle = 180 - angle;
      updateBall();
    } else if (ball.y + 15 > canvas.height || ball.y - 15 < 0) {
      angle = 360 - angle;
      updateBall();
    }
  }

  function updateBall() {
    radians = angle * Math.PI / 180;
    xUnits = Math.cos(radians) * speed;
    yUnits = Math.sin(radians) * speed;
  }

  const speed = 5;
  const pos1 = {x: 20, y: 20};
  let angle = 50;
  let radians = 0;
  let xUnits = 0;
  let yUnits = 0;

  const ball = {x: pos1.x, y: pos1.y};
  const points = new Array();

  updateBall();

  function gameLoop() {
    //window.setTimeout(gameLoop, 10);
    drawScreen();
    requestAnimationFrame(gameLoop);
  }
  gameLoop();
}
canvasApp();

